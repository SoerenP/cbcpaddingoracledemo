﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace CBCPaddingOracleDemo
{
    public class AES
    {
        private readonly CipherMode _modeOfOperation;
        private readonly PaddingMode _paddingMode;

        public AES(CipherMode modeOfOperation, PaddingMode padding)
        {
            _modeOfOperation = modeOfOperation;
            _paddingMode = padding;
        }

        public Message Encrypt(byte[] plaintext, byte[] key, byte[] iv)
        {
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.Mode = _modeOfOperation;
                aes.Padding = _paddingMode;
                aes.Key = key;
                aes.IV = iv;

                using (MemoryStream ms = new MemoryStream())
                using (var encryptor = aes.CreateEncryptor())
                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(plaintext, 0, plaintext.Length);
                    cs.FlushFinalBlock();
                    return new Message(ms.ToArray(), iv);
                }
            }
        }

        public byte[] Decrypt(Message message, byte[] key)
        {
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.Mode = _modeOfOperation;
                aes.Padding = _paddingMode;
                aes.Key = key;
                aes.IV = message.IV;

                using (MemoryStream ms = new MemoryStream())
                using (var decryptor = aes.CreateDecryptor())
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    cs.Write(message.Ciphertext, 0, message.Ciphertext.Length);
                    cs.FlushFinalBlock();
                    return ms.ToArray();
                }
            }
        }


    }
}

﻿using System;
using System.Threading;
using static CBCPaddingOracleDemo.CBCPaddingOracleAttacker;

namespace CBCPaddingOracleDemo
{
    class Program
    {
        public static Report report = (m) => 
            {
                Thread.Sleep(200);
                Console.WriteLine(m);
            };

        static void Main(string[] args)
        {
            var oracle = new Oracle();
            int blocksizeBytes = 16;

            var stolenCiphertext = oracle.GetRandomCiphertext();
            bool isPaddingValid = oracle.IsValid(stolenCiphertext);

            var attackResult = CBCPaddingOracleAttacker.Attack(stolenCiphertext, oracle.IsValid, blocksizeBytes, report);
            var plaintext = System.Text.Encoding.UTF8.GetString(attackResult);

            Console.WriteLine();
            Console.WriteLine("Result of attack: " + plaintext);
            Console.ReadLine();
        }
    }
}

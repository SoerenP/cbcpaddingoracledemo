﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBCPaddingOracleDemo
{
    public class Message
    {
        public byte[] Ciphertext { get; set; }
        public byte[] IV { get; set; }

        public Message(Message toCopy)
        {
            this.Ciphertext = new byte[toCopy.Ciphertext.Length];
            Array.Copy(toCopy.Ciphertext, Ciphertext, Ciphertext.Length);

            this.IV = new byte[toCopy.IV.Length];
            Array.Copy(toCopy.IV, IV, IV.Length);
        }

        public Message(byte[] ciphertext, byte[] iv)
        {
            this.Ciphertext = new byte[ciphertext.Length];
            Array.Copy(ciphertext, Ciphertext, Ciphertext.Length);

            this.IV = new byte[iv.Length];
            Array.Copy(iv, IV, IV.Length);
        }
    }
}

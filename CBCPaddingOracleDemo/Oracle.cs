﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CBCPaddingOracleDemo
{
    public class Oracle
    {
        public Oracle()
        {
            _paddingOracleFixedKey = RandomBytes(32);
            _aes = new AES(CipherMode.CBC, PaddingMode.PKCS7);
        }

        //TODO: other plaintexts?
        //private readonly static string[] plaintexts = new string[]
        //{
        //        "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
        //        "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
        //        "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
        //        "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
        //        "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
        //        "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
        //        "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
        //        "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
        //        "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
        //        "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"
        //};

        private readonly static string[] plaintexts = new string[]
        {
                "SnVzdCBhIHNtYWxsIHRvd24gZ2lybCwgbGl2aW5nIGluIGEgbG9uZWx5IHdvcmxk",
                "Tm8gdGltZSBmb3IgbG9zZXJzLCAnY3VzIHdlIGFyZSB0aGUgY2hhbXBpb25zDQo=",
                "WW91J3JlIHNvIHZhaW4sIHlvdSBwcm9iYWJseSB0aGluayB0aGlzIHNvbmcgaXMgYWJvdXQgeW91DQoNCg==",
                "SSBmb2xsb3cgdGhlIE1vc2t2YSwgRG93biB0byBHb3JreSBQYXJr",
                "WW91J3JlIHdvbmRlcmluZyB3aG8gSSBhbSAoU2VjcmV0LCBzZWNyZXQsIEkndmUgZ290IGEgc2VjcmV0KQ==",
                "WW91IGJldHRlciBzaGFwZSB1cCwgJ2NhdXNlIEkgbmVlZCBhIG1hbiwgQW5kIG15IGhlYXJ0IGlzIHNldCBvbiB5b3U=",
                "QW5kIEkgZ3Vlc3MgdGhhdCdzIHdoeSB0aGV5IGNhbGwgaXQgdGhlIGJsdWVzDQo=",
        };

        

        private readonly byte[] _paddingOracleFixedKey;
        private AES _aes;

        private static readonly Random getRandom = new Random();
        private static readonly RNGCryptoServiceProvider secureGetRandom = new RNGCryptoServiceProvider();

        public static byte[] RandomBytes(int length)
        {
            byte[] key = new byte[length];
            secureGetRandom.GetBytes(key);
            return key;
        }

        public Message GetRandomCiphertext()
        {
            int plaintextIndex = getRandom.Next(0, plaintexts.Length);
            var plaintext = Convert.FromBase64String(plaintexts[plaintextIndex]);
            return _aes.Encrypt(plaintext, _paddingOracleFixedKey, RandomBytes(16));
        }

        public bool IsValid(Message message)
        {
            bool result = false;

            try
            {
                var plaintext = _aes.Decrypt(message, _paddingOracleFixedKey);
                result = true;
            }
            catch(CryptographicException) /* supress all crypto warnings, yay */
            {
                result = false;
            }

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CBCPaddingOracleDemo.Program;

namespace CBCPaddingOracleDemo
{
    public class CBCPaddingOracleAttacker
    {
        public delegate void Report(String message);
        public delegate bool PaddingOracle(Message message);

        public static byte[] Attack(Message message, PaddingOracle isPaddingValid, int blocksize, Report report)
        {
            Message workingCopy = new Message(message);

            int blockCount = message.Ciphertext.Length / blocksize;
            byte[] plaintext = new byte[message.Ciphertext.Length];

            for (int b = 0; b < blockCount - 1; b++)
            {
                report("\n Attacking block "+ (blockCount - b).ToString() + "\n");

                var attackedBlock = workingCopy.Ciphertext.Reverse().Skip(b * blocksize).Take(blocksize).Reverse().ToArray();
                var priorBlock = workingCopy.Ciphertext.Reverse().Skip((b + 1) * blocksize).Take(blocksize).Reverse().ToArray();

                var plaintextBlock = CBCPaddingAttackOnSingleBlock(attackedBlock, priorBlock, isPaddingValid, blocksize, report);

                Array.Copy(plaintextBlock, 0, plaintext, plaintext.Length - ((b + 1) * blocksize), blocksize);
            }

            report("\n Attacking first block using iv \n");

            // Last block needs iv as previous, so handle it seperately. 
            var LastAttackedBlock = workingCopy.Ciphertext.Take(blocksize).ToArray();
            var LastPriorBlock = workingCopy.IV;
            var lastPlaintextBlock = CBCPaddingAttackOnSingleBlock(LastAttackedBlock, LastPriorBlock, isPaddingValid, blocksize, report);
            Array.Copy(lastPlaintextBlock, 0, plaintext, 0, blocksize);

            return plaintext;
        }

        /// <summary>
        /// Only works for the last byte of the last block currently. Need to generalize. But you gotta start somewhere, its quite the bit fiddling,
        /// and byte xor in C# is a fickle mistress.. 
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="isPaddingValid"></param>
        /// <returns></returns>
        public static byte[] CBCPaddingAttackOnSingleBlock(byte[] attackedBlock, byte[] priorBlock, PaddingOracle isPaddingValid, int blocksize, Report report)
        {
            byte[] plaintextBlock = new byte[blocksize];

            var forgedMessage = new Message(attackedBlock, priorBlock);

            int upper = blocksize + 1;

            for (int b = 1; b < upper; b++)
            {
                byte paddingByte = Convert.ToByte(b);
                byte plaintextByte = 0x00;

                // Guess the value of that plaintext byte - when padding is valid, we found it?
                for (short i = 0; i < 256; i++)
                {
                    /* Copy the Ci-1 | Ci */
                    var current = new Message(forgedMessage);

                    /* increments the current byte under attack */
                    IncrementBlock(current, Convert.ToByte(i), paddingByte, blocksize - b);

                    /* Since we xor onto the padding byte, its only that result that counts. We will also hit the original ciphertext at some point since the padding byte */
                    /* and i will cancel out inside the increment function. So that would give the original padding. We want our padding of 0x01 and so on. */
                    if (isPaddingValid(current) && Convert.ToByte(DetectPaddingByte(current, isPaddingValid)) == paddingByte)
                    {
                        /* Valid padding, save the one that worked (e.g. the R block in the IV is needed for detecting the padding byte */
                        plaintextByte = Convert.ToByte(i);

                        //report("Valid padding, guess was: " + (char)plaintextByte);
                        /* for debugging */
                        break;
                    }
                }

                plaintextBlock[blocksize - b] = plaintextByte;
                report("Byte " + b.ToString() + ": \t" + System.Text.Encoding.UTF8.GetString(FormatBlockForPrinting(plaintextBlock, blocksize, b)));

                /* prepare padding for next round, since we now the plaintext of last b bytes, so do the xor yall!*/
                for (int j = 1; j <= b; j++)
                {
                    /* use orig previous block */
                    /* We know that Ci-1 xor AES(Ci) xor plaintextbyte xor 0x01 = 0x01 during decryption. So if we make Ci-1 = Ci-1 xor plaintextbyte xor 0x02 */
                    /* for example, we get 0x02 padding instead of 0x01 */

                    forgedMessage.IV[blocksize - j] = Convert.ToByte(priorBlock[blocksize - j] ^
                                                     Convert.ToByte(plaintextBlock[blocksize - j] ^
                                                     Convert.ToByte(b + 1))); // next padding byte for next round (current plus one) 

                }
            }

            return plaintextBlock;
        }

        private static void IncrementBlock(Message message, byte i, byte paddingByte, int index)
        {
            message.IV[index] ^= i;
            message.IV[index] ^= paddingByte;
        }

        private static int DetectPaddingByte(Message message, PaddingOracle isPaddingValid)
        {
            short blocksize = 16;

            var workingCopy = new Message(message);

            short terminatingI = 0;
            for (short i = 0; i < blocksize; i++)
            {
                workingCopy.IV[i] += 1;

                if (!isPaddingValid(workingCopy))
                {
                    terminatingI = i;
                    break;
                }
            }

            return blocksize - terminatingI;
        }

        private static byte[] FormatBlockForPrinting(byte[] plaintextBlock, int blockSize, int byteIndex)
        {
            var actualText = plaintextBlock.Skip(blockSize - byteIndex).Take(byteIndex).ToArray();
            var whiteSpace = Enumerable.Repeat((byte)32, blockSize - byteIndex).ToArray();
            return whiteSpace.Concat(actualText).ToArray();
        }
    }
}
